import 'package:copilot_proxy/context.dart';
import 'package:copilot_proxy/header_manager.dart';

Future<void> postHeaderUploadToken(Context context) async {
  final accessToken = context['access_token'];
  addUpdateHeaderDeviceToken(accessToken);
  context.noContent();
}
